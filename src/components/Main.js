import React from 'react'
import { Card } from 'react-rainbow-components';

function Main() {
  return (
    <div className="rainbow-p-around_large">
      <Card>
        <img
          src="https://victorgu.com/images/smiley25.gif"
          className="rainbow-p-around_xx-large rainbow-m_auto rainbow-align-content_center"
          alt="victorgu"
        />
      </Card>
    </div>
  )
}

export default Main
