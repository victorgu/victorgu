import React from 'react';
import { Container } from '@material-ui/core';
import Header from './components/Header'
import Main from './components/Main'

function App() {
  return (
    <>
      <Container maxWidth="sm">
        <Header />
        <Main />
      </Container>
    </>
  );
}

export default App;
